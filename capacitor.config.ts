import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.dillards.vault',
  appName: 'Identity Vault Demo',
  webDir: 'www',
  bundledWebRuntime: false,
};

export default config;
