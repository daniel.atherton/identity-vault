import {Injectable, NgZone} from '@angular/core';
import {Capacitor} from '@capacitor/core';
import {
  BrowserVault,
  Device,
  DeviceSecurityType,
  IdentityVaultConfig,
  Vault, VaultErrorCodes,
  VaultType,
} from '@ionic-enterprise/identity-vault';
import {ModalController, Platform} from '@ionic/angular';
import {PinComponentPage} from "./pin-component/pin-component.page";
import {environment} from "../environments/environment";

const config: IdentityVaultConfig = {
  key: 'com.dillards.vault',
  type: VaultType.DeviceSecurity,
  deviceSecurityType: DeviceSecurityType.Both,
  lockAfterBackgrounded: environment.vaultTimeout,
  shouldClearVaultAfterTooManyFailedAttempts: false,
  customPasscodeInvalidUnlockAttempts: environment.pinAttempts,
  unlockVaultOnLoad: false,
};
const key = config.key;

export interface VaultServiceState {
  isEmpty: boolean;
  isLocked: boolean;
  session: string | undefined;
  privacyScreen: boolean;
  lockType: DeviceSecurityType;
  canUseBiometrics: boolean;
  canUseSystemPasscode: boolean;
  canUseCustomPasscode: boolean;
  pin: string;
}

@Injectable({providedIn: 'root'})
export class VaultService {
  public state: VaultServiceState = {
    isEmpty: true,
    isLocked: false,
    session: '',
    privacyScreen: true,
    lockType: DeviceSecurityType.None,
    canUseBiometrics: false,
    canUseSystemPasscode: false,
    canUseCustomPasscode: true,
    pin: ''
  };

  vault: Vault | BrowserVault;

  constructor(private ngZone: NgZone, private platform: Platform, private modalController: ModalController) {
    this.vault = Capacitor.getPlatform() === "web" ? new BrowserVault(config) : new Vault(config);
  }

  async init() {
    await this.platform.ready(); // This is required only for Cordova

    this.vault.onLock(() => {
      console.debug(`[onLock()]`);
      this.ngZone.run(() => {
        this.state.isLocked = true;
        this.state.session = undefined;
      });
    });

    this.vault.onPasscodeRequested((isPasscodeSetRequest: boolean) => {
      console.debug(`[onPasscodeRequested()]:\n%o`, isPasscodeSetRequest);
      this.modalController.create({
        backdropDismiss: false,
        component: PinComponentPage,
        componentProps: {isPasscodeSetRequest: isPasscodeSetRequest, pin: this.state.pin},
      }).then((pinComponent) => {
        pinComponent.onWillDismiss().then(async (data) => {
          const currentPin: string = this.state.pin;
          const enteredPin: string = data.data;
          console.debug(`[onPasscodeRequested()]\nCurrent PIN: ${currentPin}\n Entered PIN: ${enteredPin}`);
          if (isPasscodeSetRequest) {
            this.state.pin = data.data;
            await this.vault.setCustomPasscode(this.state.pin || '');
          } else if (currentPin !== data.data) {
            console.debug(`[onPasscodeRequestedAndPinAlreadySet()]\nPIN Already Set: ${this.vault.isEmpty()}\nCurrent PIN: ${currentPin}: Entered PIN: ${data.data}`);
            await this.vault.lock();
          } else {
            this.state.pin = data.data;
          }
        });
        return pinComponent.present();
      });
    })

    this.vault.onUnlock(() => {
      console.debug(`[onUnlock()]`);
      this.ngZone.run(async () => {
        this.state.isLocked = false;
        if (!(await this.vault.isEmpty())) {
          try {
            await this.restoreSession();
          } catch (e: any) {
            console.error(`[TryUnlockException()] %o`, e);
            switch (e.code) {
              case VaultErrorCodes.AuthFailed:
                break;
              case VaultErrorCodes.UserCanceledInteraction:
                alert('You cancelled the face id prompt!');
                break;
              default:
                throw e;
            }
          }
        }
      });
    });

    this.state.isEmpty = (await this.vault.isEmpty());
    this.state.isLocked = (await this.vault.isLocked());

    this.state.privacyScreen = Capacitor.getPlatform() === "web" ? false : await Device.isHideScreenOnBackgroundEnabled();

    this.state.canUseCustomPasscode = Capacitor.getPlatform() === "web" ? true : !(await Device.isBiometricsAllowed() || await Device.isSystemPasscodeSet());
    this.state.canUseBiometrics = Capacitor.getPlatform() === "web" ? false : await Device.isBiometricsEnabled();
    this.state.canUseSystemPasscode = Capacitor.getPlatform() === "web" ? false : await Device.isSystemPasscodeSet();
  }

  async clearVault() {
    await this.vault.clear();
    this.state.lockType = DeviceSecurityType.None;
    this.state.session = undefined;
    this.state.isEmpty = (await this.vault.isEmpty());
  }

  async setSession(value: string | undefined): Promise<void> {
    this.state.session = value;
    await this.vault.setValue(key, value);
    this.state.isEmpty = (await this.vault.isEmpty());
  }

  async restoreSession() {
    const value = (await this.vault.getValue(key));
    this.state.session = value;
  }

  async lockVault() {
    await this.vault.lock();
  }

  async unlockVault() {
    await this.vault.unlock();
  }

  setPrivacyScreen(enabled: boolean) {
    Device.setHideScreenOnBackground(enabled);
    this.state.privacyScreen = enabled;
  }

  async setLockType() {
    let type: VaultType;
    let deviceSecurityType: DeviceSecurityType;

    switch (this.state.lockType) {
      case DeviceSecurityType.Biometrics:
        type = VaultType.DeviceSecurity;
        deviceSecurityType = DeviceSecurityType.Biometrics;
        break;
      case DeviceSecurityType.SystemPasscode:
        type = VaultType.DeviceSecurity;
        deviceSecurityType = DeviceSecurityType.SystemPasscode;
        break;
      default:
        type = VaultType.CustomPasscode;
        deviceSecurityType = DeviceSecurityType.None;
        break;
    }
    await this.vault.updateConfig({...this.vault.config, type, deviceSecurityType} as IdentityVaultConfig);
  }
}
