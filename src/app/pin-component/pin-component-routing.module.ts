import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PinComponentPage } from './pin-component.page';

const routes: Routes = [
  {
    path: '',
    component: PinComponentPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PinComponentPageRoutingModule {}
