import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PinComponentPageRoutingModule } from './pin-component-routing.module';

import { PinComponentPage } from './pin-component.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PinComponentPageRoutingModule
  ],
  declarations: [PinComponentPage]
})
export class PinComponentPageModule {}
