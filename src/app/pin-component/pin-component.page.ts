import {Component, Input, OnInit, Output} from '@angular/core';
import {ModalController} from "@ionic/angular";

@Component({
  selector: 'app-pin-component',
  templateUrl: './pin-component.page.html',
  styleUrls: ['./pin-component.page.scss'],
})
export class PinComponentPage implements OnInit {
  @Output() pin: string = '';
  // @ts-ignore
  @Input() isPasscodeSetRequest: boolean;

  constructor(public modalController: ModalController) {
  }

  ngOnInit() {
    this.pin = '';
  }

  setPin() {
    this.modalController.dismiss(this.pin)
  }


}
