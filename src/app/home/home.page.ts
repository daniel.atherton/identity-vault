import {Component} from '@angular/core';
import {VaultService, VaultServiceState} from '../vault.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  public state: VaultServiceState;

  constructor(private vaultService: VaultService) {
    this.state = vaultService.state;
  }

  async setSession(data: string | undefined) {
    await this.vaultService.setSession(data);
  }

  async restoreSession() {
    await this.vaultService.restoreSession();
  }

  async lockVault() {
    await this.vaultService.lockVault()
  }

  async unlockVault() {
    await this.vaultService.unlockVault()
  }

  setPrivacyScreen() {
    this.vaultService.setPrivacyScreen(this.state.privacyScreen)
  }

  setLockType() {
    this.vaultService.setLockType().finally();
  }

  clearVault() {
    this.vaultService.clearVault().finally();
  }
}
