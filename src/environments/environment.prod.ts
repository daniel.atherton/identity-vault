export const environment = {
  production: true,
  ignorePin: false,
  vaultTimeout: 10000,
  pinAttempts: 25,
  pinAttemptsBeforeLockout: 5,
  pinLockout: [45000, 60000, 75000]
};
